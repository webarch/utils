#!/usr/bin/env python3
"""
This module contiains utility functions to manage adding SPF, DKIM and
DMARC records to Webarchitects DNS zones

Example:
    $ python3 dkimSPF.py
"""

from datetime import datetime
from dns import name, rdata, rdataclass, rdatatype, zone
from sys import argv

def getZoneFile(bindFilePath):
    '''
    Takes a file path, and read's the bind zone file at that location
    assumes zoneFiles are all called domain.name.hosts
    '''
    bindZoneFile = bindFilePath.split('/')[-1]
    bindDomainName = '.'.join(bindZoneFile.split('.')[:-1])
    return zone.from_file(bindZoneFile, bindDomainName)

def writeZoneFile(zf, filePath):
    '''
    outputs a zone to bind format text file
    '''
    return zf.to_file(filePath)

def getDKIMRecord(zf, selectorText):
    '''
    Takes a DKIM Selector and returns the DKIM TXT record
    '''
    dkimSelector = name.Name((selectorText, '_domainkey',))
    dkimRecords = zf.get_rdataset(dkimSelector, rdatatype.from_text('TXT'))
    return dkimRecords

def setDKIMRecord(zf, selectorText, dkimString, replace=True):
    '''
    Given a DKIM Selector, DKIM TXT string, modifies the zone to use that value
    '''
    dkimSelector = name.Name((selectorText, '_domainkey',))
    dkimRecords = zf.get_rdataset(dkimSelector, rdatatype.from_text('TXT'), create=True)
    if replace is True:
        dkimRecords.clear()
    txtValue = rdata.from_text(rdataclass.from_text('IN'), rdatatype.TXT, dkimString)
    dkimRecords.add(txtValue)
    return

def genDKIMString(pubKey):
    ''' 
    Given a DKIM pubKey generate a default DKIM TXT string
    Split the TXT String if it is too long for bind
    '''
    default = 'v=DKIM1; k=rsa; t=s; s=email; p='
    maxRecordLength = 215
    returnValue = default + pubKey
    return " ".join(['"' + returnValue[i:i + maxRecordLength] +'"' for i in range(0, len(returnValue), maxRecordLength)])

def getSPFRecord(zf):
    '''
    Returns the current zone SPF record
    '''
    txtRecords = zf.get_rdataset(zf.origin, rdatatype.from_text('TXT'))
    spfRecord = []
    if txtRecords != None:
        spfRecords = [x for x in txtRecords.items if x.to_text().startswith('"v=spf1')]
        if spfRecords != []:
            spfRecord = spfRecords[0]
    return spfRecord

def setSPFRecord(zf, spf='"v=spf1 a mx include:_spf.webarch.email ~all"', replace=False):
    '''
    Writes a webarchitects default SPF record
    '''
    if getSPFRecord(zf) == [] or replace:
        txtRecords = zf.get_rdataset(zf.origin, rdatatype.from_text('TXT'), create=True)
        spfRecord = [x for x in txtRecords.items if x.to_text().startswith('"v=spf1')]
        if spfRecord:
            txtRecords.discard(spfRecord[0])
        txtValue = rdata.from_text(rdataclass.from_text('IN'), rdatatype.TXT, spf)
        txtRecords.add(txtValue)
    return

def getDMARCRecord(zf):
    '''
    Get an existing DMARC record
    '''
    dmarcSelector = name.Name(('_dmarc',))
    dmarcRecords = zf.get_rdataset(dmarcSelector, rdatatype.TXT)
    return dmarcRecords

def setDMARCRecord(zf, dmarcString, replace=True):
    '''
    Given a DMARC TXT string, modifies the zone to use that value
    '''
    dmarcSelector = name.Name(('_dmarc',))
    dmarcRecords = zf.get_rdataset(dmarcSelector, rdatatype.TXT, create=True)
    if replace is True:
        dmarcRecords.clear()
    txtValue = rdata.from_text(rdataclass.IN, rdatatype.TXT, dmarcString)
    dmarcRecords.add(txtValue)
    return

def getZoneSerial(zf):
    ''' 
    Get the current Serial Number of a Zone
    '''
    zoneSOADatas = zf.get_rdataset('@',rdatatype.from_text('SOA'))
    zoneSOA = zoneSOADatas.items[0] # there is only a single SOA record
    return zoneSOA.serial 

def incrementZoneSerial(zf):
    ''' 
    Increment the current Zone serial by 1
    '''
    zoneSOADatas = zf.get_rdataset('@',rdatatype.from_text('SOA'))
    zoneSOA = zoneSOADatas.items[0] # there is only a single SOA record
    # bump the serial by 1, this writes in place the in memory Zonefile
    zoneSOA.serial += 1
    return 

def niceIncrementZoneSerial(zf):
    '''
    Increment the zone serial, but using the YYYYMMDDxx convention
    '''
    minSerialNum = int(datetime.now().strftime("%Y%m%d")) * 100 + 1
    zoneSOADatas = zf.get_rdataset('@',rdatatype.from_text('SOA'))
    zoneSOA = zoneSOADatas.items[0] # there is only a single SOA record
    if zoneSOA.serial > minSerialNum - 1:
        zoneSOA.serial += 1
    else:
        zoneSOA.serial = minSerialNum
    return

    
def main():
    if len(argv) != 4: 
        print("Usage: ", argv[0], '/path/to/bindZoneFile', 'DKIM_Selector', 'DKIM_PubKey')
        exit(-1)
    bindZoneFile = argv[1]
    selectorText = argv[2]
    pubKey = argv[3]
    zf = getZoneFile(bindZoneFile)
    # example set SPF record ( if one does not exist), and set DKIM, and DMARC records
    setSPFRecord(zf)
    setDKIMRecord(zf, selectorText, genDKIMString(pubKey))
    defaultDMARC = '"v=DMARC1; p=none; rua=mailto:dmarc-webarchclient-aggregate@webarchitects.coop; ruf=mailto:dmarc-webarchclient-forensics@webarchitects.coop; fo=1"'
    setDMARCRecord(zf, defaultDMARC)
    niceIncrementZoneSerial(zf)
    writeZoneFile(zf, bindZoneFile)

if __name__ == '__main__':
    main()

