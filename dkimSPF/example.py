#!/usr/bin/env python3
'''
Example of using dkimSPF tools within python to
1. Read a file (keys.txt)  containing  a list of domains and dkim pub keys formatted
    domain.example.com, "DKIM PUB KEY STRING......"

2. read the zone files associated with the domains

3. Add the SPF record, DKIM records, DMARC recors
'''

from dkimSPF import * 

def main():
    with open('keys.txt', 'r') as f:
        data=f.readlines()
    f.close()
    data = [x.strip() for x in data]
    for l in data:
        domain,key = map(lambda x: x.strip(), l.split(","))
        bindZoneFile = domain + '.hosts'
        zf = getZoneFile(bindZoneFile)
        # example set DKIM, SPF, DMARC, and increment the zoneFile before saving.
        if key[0] == '"' and key[-1] == '"':
            pubKey = key[1:-1]
        else:
            pubKey = key
        selectorText = "20190710"
        setDKIMRecord(zf, selectorText, genDKIMString(pubKey))
        setSPFRecord(zf)
        defaultDMARC = '"v=DMARC1; p=none; rua=mailto:dmarc-webarchclient-aggregate@webarchitects.coop; ruf=mailto:dmarc-webarchclient-forensics@webarchitects.coop; fo=1"'
        setDMARCRecord(zf, defaultDMARC)
        niceIncrementZoneSerial(zf)
        writeZoneFile(zf, bindZoneFile)

if __name__ == '__main__':
    main()

