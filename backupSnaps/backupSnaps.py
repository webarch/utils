#!/usr/bin/env python3
import argparse, os, shutil, subprocess, sys, yaml
from pathlib import Path

def diskImageExists(host, diskImage, snapDir, snap):
    p = Path(snapDir)
    return (p / snap / 'domains' / host / diskImage).is_file()

def mountPointExists(exportRoot, host, diskImage, snapDir, snap):
    return (Path(exportRoot) / host / snap / diskImage).is_dir()

def userDirExists(user):
    return (Path('/home') / user).is_dir()

def userSnapshotDirExists(exportRoot, host, user, diskImage, snapDir, snap):
    return (Path('/home') / user / 'snapshots' / snap / diskImage).is_dir()

def makeMountPoint(exportRoot, host, user, diskImage, snapDir, snap):
    if diskImageExists(host, diskImage, snapDir, snap):
        p = Path(exportRoot) / host / snap / diskImage
        p.mkdir(parents=True, exist_ok=True)
    return

def makeUserMountPoint(exportRoot, host, user, diskImage, snapDir, snap):
    if diskImageExists(host, diskImage, snapDir, snap) and userDirExists(user):
        p = Path('/home/' + user + '/snapshots') / snap / diskImage
        p.mkdir(parents=True, exist_ok=True)
    return

def mountImage(exportRoot, host, user, diskImage, snapDir, snap):
    mountOptions = ['-o', 'loop,ro,noexec,noload']
    if diskImageExists(host, diskImage, snapDir, snap) and mountPointExists(exportRoot, host, diskImage, snapDir, snap):
        imagePath = Path(snapDir) / snap / 'domains' / host / diskImage
        strImagePath = str(imagePath)
        mountPoint = Path(exportRoot) / host / snap / diskImage
        strMountPoint = str(Path(exportRoot) / host / snap / diskImage)
        args = ['/bin/mount'] + mountOptions + [strImagePath, strMountPoint]
        p = subprocess.Popen(args)
        p.wait()
    return

def mountUserImage(exportRoot, host, user, diskImage, snapDir, snap):
    # print (userSnapshotDirExists(exportRoot, host, user, diskImage, snapDir, snap))
    # print (isMounted(exportRoot, host, user, diskImage, snapDir, snap))
    if userSnapshotDirExists(exportRoot, host, user, diskImage, snapDir, snap) and isMounted(exportRoot, host, user, diskImage, snapDir, snap):
        mountPoint = Path(exportRoot) / host / snap / diskImage
        strMountPoint = str(mountPoint)
        userMountPoint = Path('/home') / user / 'snapshots' / snap / diskImage 
        strUserMountPoint = str(userMountPoint)
        args = ['/usr/bin/bindfs'] + ['-u', user] + ['-g', user] + [strMountPoint, strUserMountPoint]
        # print (args)
        p = subprocess.Popen(args)
        p.wait()
    return

def isMounted(exportRoot, host, user, diskImage, snapDir, snap):
    # print (mountPointExists(exportRoot, host, diskImage, snapDir, snap))
    if mountPointExists(exportRoot, host, diskImage, snapDir, snap):
        mountPoint = Path(exportRoot) / host / snap / diskImage
        return mountPoint.is_mount()
    return False
         
def mountAll(conf):
    hosts = conf['hosts']
    snapDir = conf['snapDir']
    exportRoot = conf['exportRoot']
    snapshots = set([x for x in os.listdir(snapDir)])
    for host in hosts:
        user = hosts[host]['user']
        # print(user)
        for diskImage in hosts[host]['disks']:
            for snap in snapshots:
                # print(host, diskImage, snap)
                makeMountPoint(exportRoot, host, user, diskImage, snapDir, snap)
                makeUserMountPoint(exportRoot, host, user, diskImage, snapDir, snap)
                mountImage(exportRoot, host, user, diskImage, snapDir, snap)
                mountUserImage(exportRoot, host, user, diskImage, snapDir, snap)
    return

def umountAllUser(conf):
    hosts = conf['hosts']
    for host in hosts:
        user = hosts[host]['user']
        pth = Path('/home') / user / 'snapshots'
        if pth.exists():
            subDir1 = [x for x in os.listdir('/home/' + user + '/snapshots')]
            for snap in subDir1:
                subDir2 = [x for x in os.listdir('/home/' + user + '/snapshots' + '/' + snap)]
                for disk in subDir2:
                    p = Path('/home') / user / 'snapshots' / snap / disk
                    if p.is_mount():
                        ps = subprocess.Popen(['/bin/umount', '-f' , str(p)])
                        ps.wait()
    return 

def umountAllLoop(conf):
    exportRoot = conf['exportRoot']
    subDir1 = [x for x in os.listdir(exportRoot)] 
    for host in subDir1:
        subDir2 = [x for x in os.listdir(exportRoot + "/" + host)]
        for snap in subDir2:
            subDir3 = [x for x in os.listdir(exportRoot + "/" + host + "/" + snap)]
            for disk in subDir3:
                p = Path(exportRoot) / host / snap / disk
                if p.is_mount():
                    ps = subprocess.Popen(['/bin/umount', '-f' , str(p)])
                    ps.wait()
    return

def removeLoopMountDir(conf):
    exportRoot = conf['exportRoot']
    if exportRoot == '' or exportRoot[0:3] == "../" :
        return
    subDir1 = [x for x in os.listdir(exportRoot)] 
    for host in subDir1:
        if host == '' or host[0:3] == "../":
            return
        subDir2 = [x for x in os.listdir(exportRoot + "/" + host)]
        shutil.rmtree(exportRoot + "/" + host)
    return 

def removeUserMountDir(conf):
    hosts = conf['hosts']
    for host in hosts:
        user = hosts[host]['user']
        if user == '' or user[0:3] == "../":
            return
        p = Path('/home') / user / 'snapshots'
        if p.exists():
            shutil.rmtree('/home' + '/' + user + '/snapshots')
    return 

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--config", help="path to config file", )
    parser.add_argument("action", help="action is umountAllUser | umountAllLoop | removeLoopMountDir | removeUserMountDir | removeAll | mountAll")

    args = parser.parse_args()
    execpath = (os.path.dirname(__file__))
    conf = yaml.load(open(args.config)) if args.config else yaml.load(open(execpath + "/conf/conf.yml"))
    if args.action == "umountAllUser":
        umountAllUser(conf)
    elif args.action == "umountAllLoop":
        umountAllLoop(conf)
    elif args.action == "removeLoopMountDir":
        removeLoopMountDir(conf)
    elif args.action == "removeUserMountDir":
        removeUserMountDir(conf)
    elif args.action == "removeAll":
        umountAllUser(conf)
        umountAllLoop(conf)
        removeLoopMountDir(conf)
        removeUserMountDir(conf)
    elif args.action == "mountAll":
        mountAll(conf)
    else:
        print("Unsupported operation")
        sys.exit(1)


if __name__ == '__main__':
    main() 
        
