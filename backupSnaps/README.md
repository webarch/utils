# BackupSnaps

.zfs snapshots are in the following order

.zfs/snapshots/snapshot@date/../../../virtual_machine_disk.img

this mounts the opaque disk images, and remounts them by hostname and date

eg. 

../dir/hostname/date/disk.img/contents of the image

then mounts them all again, so root@virtual_machine from the virtual machine can access them via sshfs 
without being able to access any of the others.
