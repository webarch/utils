#!/usr/bin/env python3

import argparse, os, shutil, subprocess, sys, yaml
from pathlib import Path

def getCurrentUsers(conf):
    users = []
    usersConfDir = conf['usersConfDir']
    for f in os.listdir(usersConfDir):
        if f.endswith(".yml"):
            p = Path(usersConfDir) / f
            userconf = yaml.load(open(p)) 
            if 'users_groups' in userconf and 'users_state' in userconf:
                if 'chroot' in userconf['users_groups'] and userconf['users_state'] == 'present':
                    username = f.split('.yml')[0]
                    users.append(username)
    return users

def getAvailableSnaps(conf):
    snapImportDir = conf['snapsImportDir']
    return sorted([d for d in os.listdir(snapImportDir) if os.path.isdir(os.path.join(snapImportDir, d))])

def userSnapExists(conf, user, snap):
    backupPrefix = conf['backupPrefix']
    snapsImportDir = conf['snapsImportDir']
    userSnap = os.path.join(snapsImportDir, snap, backupPrefix, user)
    # print(userSnap)
    return os.path.isdir(userSnap)

def createMountPoint(conf, user, snap):
    p = Path('/home') / user / 'backups' / snap
    p.mkdir(parents=True, exist_ok=True)
    return 

# def rmMountPoint(conf, user, snap):
#     p = Path('/home') / user / 'backups' / snap
#     p.rmdir()
#     return

def mkBindMount(conf, user, snap):
    # mount -obind /srv/import/backups/auto-UTC-2019-12-20_02.00/home.img/drupal /users/drupal/home/drupal/backups/auto-UTC-2019-12-20_02.00/
    backupPrefix = conf['backupPrefix']
    snapsImportDir = conf['snapsImportDir']
    userSnap = os.path.join(snapsImportDir, snap, backupPrefix, user)
    mountPoint = os.path.join('/users', user, 'home', user, 'backups', snap)
    mountOptions = ['-o', 'bind']
    args = ['/bin/mount'] + mountOptions + [userSnap, mountPoint]
    p = subprocess.Popen(args)
    p.wait()
    return

def mountAllUserBackupSnaps(conf):
    users = getCurrentUsers(conf)
    snaps = getAvailableSnaps(conf)
    for user in users:
        for snap in snaps:
            if userSnapExists(conf, user, snap):
                createMountPoint(conf, user, snap)
                mkBindMount(conf, user, snap)
    return

def umountAllUSerBackupSnaps(conf):
    # list all users in /users
    for user in os.listdir('/users'):
        print(user)
        p = Path('/users') / user / 'home' / user / 'backups'
        for mountPoint in os.listdir(p):
            print ((p / mountPoint).is_mount())
            # if ((p / mountPoint).is_mount()):
            #      print(user, mountPoint)
            #      mp = p / mountPoint
            #      print(mp)
            #      umount = ['/bin/umount', '-f', mp]
            #      p = subprocess.Popen(umount)
            #      p.wait()
    #         if possibleMount.isMount():
    #             forceUmount(possibleMount)            
    #     rmtree('/home/user/backups')
    return

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--config", help="path to config file", )
    # parser.add_argument("action", help="action is umountAllUser | umountAllLoop | removeLoopMountDir | removeUserMountDir | removeAll | mountAll")

    args = parser.parse_args()
    execpath = (os.path.dirname(__file__))
    conf = yaml.load(open(args.config)) if args.config else yaml.load(open(execpath + "/conf/conf.yml"))
    # mountAllUserBackupSnaps(conf)
    print(umountAllUSerBackupSnaps(conf))

    return 

if __name__ == '__main__':
    main()

